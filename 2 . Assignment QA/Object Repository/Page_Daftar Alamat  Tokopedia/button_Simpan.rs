<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Simpan</name>
   <tag></tag>
   <elementGuidId>feacd649-446c-4175-967a-cb314d3d84b0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.css-lwa81l-unf-btn.eg8apji0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[19]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>d5475f0f-637c-41b5-afa0-0fa7c0c6a194</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-unify</name>
      <type>Main</type>
      <value>Button</value>
      <webElementGuid>d4d168e3-1d94-4465-9d74-0b648021c92c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>379472cf-d3fa-4347-a621-b70723370d98</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-lwa81l-unf-btn eg8apji0</value>
      <webElementGuid>039dc573-e9a7-40db-8857-c2c6a1d06322</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Simpan</value>
      <webElementGuid>de350148-7398-4f0a-8c51-5193a920b9f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;css-1g5w2rw&quot;]/div[14]/div[@class=&quot;css-uri0jd-unf-dialog ef541p40&quot;]/div[@class=&quot;css-1oqklfy ef541p41&quot;]/button[@class=&quot;css-lwa81l-unf-btn eg8apji0&quot;]</value>
      <webElementGuid>89685e1d-7501-4831-a7d6-21a41e0bb21a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[19]</value>
      <webElementGuid>c0b70e44-1ddc-4233-9fc5-36fee1dbc008</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/button</value>
      <webElementGuid>93f73c26-be79-4ce8-82de-bcd7cdcd899a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Simpan' or . = 'Simpan')]</value>
      <webElementGuid>32b45501-2418-435f-9268-1a11a5d94b79</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
