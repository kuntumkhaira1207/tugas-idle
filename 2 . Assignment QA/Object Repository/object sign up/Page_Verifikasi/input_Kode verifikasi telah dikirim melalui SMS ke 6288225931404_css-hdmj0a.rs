<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Kode verifikasi telah dikirim melalui SMS ke 6288225931404_css-hdmj0a</name>
   <tag></tag>
   <elementGuidId>965fbc97-db93-4107-abc9-b5e2794cbf5f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input.css-hdmj0a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>8dd3f358-c05a-409a-9d65-fa3c07f3fa5a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>tel</value>
      <webElementGuid>500c36d2-520c-4af9-b73c-0defed724fd1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-hdmj0a</value>
      <webElementGuid>a979f214-4a78-4441-8bd9-4e302063183c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>6</value>
      <webElementGuid>601f790a-5521-4932-b4a9-2fdc70098bef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>one-time-code</value>
      <webElementGuid>cba21f55-ee26-4a87-a87b-25bf01924d23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>otp input</value>
      <webElementGuid>02a23202-e37b-4f80-8923-1900b655f471</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;zeus-root&quot;)/div[@class=&quot;css-8atqhb&quot;]/div[@class=&quot;css-1pjobg4&quot;]/div[@class=&quot;css-vpkyow&quot;]/input[@class=&quot;css-hdmj0a&quot;]</value>
      <webElementGuid>093b301a-a8e2-4ed9-b8a7-fcc2793fce48</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='']</value>
      <webElementGuid>f9efe9d8-d511-4de6-b407-102f49b1e7ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='zeus-root']/div/div[2]/div/input</value>
      <webElementGuid>c4777b80-988c-4db9-a04e-a3d484478c5b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>2f5022a1-37ac-4268-9baf-12f57259c8af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'tel']</value>
      <webElementGuid>f91ff21f-1e18-4a7a-9d51-91d47267a615</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
