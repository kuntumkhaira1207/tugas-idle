import java.util.*;
    public class soaltest19 
    { 
        public static boolean checkPangram (String str) 
        { 
            int index = 0,count=0; 
            char s[]=str.toCharArray();
            Set<Character> hs= new HashSet<Character>();
            for(index=0;index<str.length();index++)
            {
                hs.add(s[index]); 
            }
            Iterator<Character> i=hs.iterator();
            while(i.hasNext())
            {
              count++;
              i.next();
            }
            if(count==26)
              return true;
            return false;
        } 

        // Driver Code 
        public static void main(String[] args) 
        { 
            Scanner input =new Scanner(System.in);
            System.out.print("Input kata : ");
            String str = input.nextLine();
           // String str = "the quick brown fox jumps over the lazy dog"; 

            if (checkPangram(str) == true) 
                System.out.print(" is a pangram."); 
            else
                System.out.print(" is not a pangram."); 

        } 
    } 