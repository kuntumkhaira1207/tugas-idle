<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_css-hdmj0a</name>
   <tag></tag>
   <elementGuidId>a4106c26-b647-4eb4-9203-9fda82c9a21f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input.css-hdmj0a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='2']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>8612614a-2897-48a5-a9d2-1230bbe405b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>tel</value>
      <webElementGuid>0a6ae943-6309-45de-bff2-2fe9ac467f54</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-hdmj0a</value>
      <webElementGuid>9dd07114-2359-4cd7-a0f6-36dad2cd881f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>6</value>
      <webElementGuid>1ec2353e-ff47-4e78-b86b-550237f05596</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>one-time-code</value>
      <webElementGuid>0197e041-6d72-44dc-b5fc-56a72852c5b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>otp input</value>
      <webElementGuid>93f5830f-219e-42c3-a012-e03b99486062</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>aba35b07-17e3-43f7-971c-d93009b2c6de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;css-1g5w2rw&quot;]/div[5]/div[@class=&quot;css-y344pu e1nc1fa20&quot;]/article[@class=&quot;css-1f0x5lg-unf-modal e1nc1fa21&quot;]/div[@class=&quot;css-18qem4c e1nc1fa22&quot;]/div[@class=&quot;css-1qwpmq4&quot;]/div[@class=&quot;css-vpkyow&quot;]/input[@class=&quot;css-hdmj0a&quot;]</value>
      <webElementGuid>0774177c-2416-455c-9f8a-27e7340979b5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='2']</value>
      <webElementGuid>485114bb-030c-4b23-bd5d-bc45201ecf2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//article/div/div/div/input</value>
      <webElementGuid>76dcd52c-553a-4465-a178-f72048627966</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'tel']</value>
      <webElementGuid>d3aa7eac-04bf-4f05-bcb9-78961ef5044c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
