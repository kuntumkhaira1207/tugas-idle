import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.tokopedia.com/')

WebUI.click(findTestObject('Object Repository/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/button_Masuk'))

WebUI.setText(findTestObject('Object Repository/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/inputemail-phone'), '088225931404')

WebUI.click(findTestObject('Object Repository/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/span_Selanjutnya'))

WebUI.click(findTestObject('Object Repository/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/button_Gunakan Metode SMS'))

WebUI.setText(findTestObject('Object Repository/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/input_css-3017qm exxxdg63'), 
    '705673')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/input_css-3017qm exxxdg63'), 
    'CSt51UwWECc=')

WebUI.setText(findTestObject('Object Repository/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/input_css-3017qm exxxdg63'), 
    'kalung')

WebUI.click(findTestObject('Object Repository/Page_Situs Jual Beli Online Terlengkap, Mud_c2d1e5/strong_kalung di Roy Stone Official'))

WebUI.click(findTestObject('Object Repository/Page_Produk dan Etalase dari Roy Stone Offi_1475f8/img_css-1q90pod'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/button_Beli Langsung'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/div_BCA Virtual Account'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/svg_unf-icon'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/div_BCA Virtual Account'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/div_BCA Virtual Account'))

WebUI.doubleClick(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/div_BCA Virtual Account'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/div_BCA Virtual Account'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/svg_unf-icon'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/divduration-dropdown'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/div_Reguler (Rp8.000 - Rp11.500)Estimasi ti_d02d61'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/svg_unf-icon'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/div_Rp0'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/svg_unf-icon'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/div_BCA Virtual Account'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/div_Bayar'))

WebUI.setText(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/input_css-3017qm exxxdg63'), 
    'kalung')

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/strong_kalung di Roy Stone Official'))

WebUI.click(findTestObject('Object Repository/Page_Produk dan Etalase dari Roy Stone Offi_1475f8/img_css-1q90pod'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/button_Beli Langsung'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/div_BCA Virtual Account'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/div_Saldo tidak cukup'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/svg_unf-icon'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/div_BCA Virtual Account'))

WebUI.click(findTestObject('Object Repository/Page_Promo RoyStone - Kalung Wanita Moissan_0a848b/div_Bayar'))

WebUI.closeBrowser()

