<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>strong_kalung di Roy Stone Official</name>
   <tag></tag>
   <elementGuidId>ee92ba45-2d04-4900-a859-f7b89aa8e215</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='header-main-wrapper']/div[2]/div[2]/div[3]/a/div/span/strong</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>18728791-a42e-485b-a239-f481299acd82</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>kalung di Roy Stone Official</value>
      <webElementGuid>29642b41-1d5f-4bea-b69b-d684fcf5cba5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;header-main-wrapper&quot;)/div[@class=&quot;css-12jp264 e90swyx2&quot;]/div[@class=&quot;css-1a6ytj8&quot;]/div[@class=&quot;css-1471acr&quot;]/a[@class=&quot;css-ey394k&quot;]/div[@class=&quot;css-1qprk9z&quot;]/span[@class=&quot;css-1c9ttf1&quot;]/strong[1]</value>
      <webElementGuid>4a07b424-d93f-4208-97ed-8bc760f79137</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='header-main-wrapper']/div[2]/div[2]/div[3]/a/div/span/strong</value>
      <webElementGuid>4374d338-0005-43c9-9bfa-8240562f3bcb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//strong</value>
      <webElementGuid>d5295d57-a920-4041-a38d-cf33fbd6a864</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = 'kalung di Roy Stone Official' or . = 'kalung di Roy Stone Official')]</value>
      <webElementGuid>b6e043b6-60f9-45ec-9c18-735257966131</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
