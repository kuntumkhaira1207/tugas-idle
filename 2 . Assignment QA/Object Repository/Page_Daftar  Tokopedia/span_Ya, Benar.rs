<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Ya, Benar</name>
   <tag></tag>
   <elementGuidId>6a2ea367-d1ba-4b72-8695-aa565a40e928</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Apakah nomor HP yang Anda masukkan sudah benar?'])[1]/following::span[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.css-lwa81l-unf-btn.eg8apji0 > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>9787313d-e074-4780-90d4-f87832bf8392</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ya, Benar</value>
      <webElementGuid>304253de-00b9-4019-a5b1-3bbd1530ce95</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;css-18e1z3z&quot;]/body[1]/div[3]/div[@class=&quot;css-17b17is-unf-dialog ef541p40&quot;]/div[@class=&quot;css-1oqklfy ef541p41&quot;]/button[@class=&quot;css-lwa81l-unf-btn eg8apji0&quot;]/span[1]</value>
      <webElementGuid>48c26556-bd65-489a-8508-beda8dfe1695</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apakah nomor HP yang Anda masukkan sudah benar?'])[1]/following::span[1]</value>
      <webElementGuid>c44f2422-4c98-4883-b7af-a99c3aea3f93</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tokopedia Care'])[1]/following::span[2]</value>
      <webElementGuid>8a5ca3d8-d676-43a5-951c-4feeb303459a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ubah'])[1]/preceding::span[1]</value>
      <webElementGuid>98803531-b76e-481a-90c0-1aabb2c47692</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Ya, Benar']/parent::*</value>
      <webElementGuid>d6d8dab6-b6a6-4509-a513-67894db6faa6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button/span</value>
      <webElementGuid>fb044108-20bb-460b-9c74-f81ee44f278e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Ya, Benar' or . = 'Ya, Benar')]</value>
      <webElementGuid>0066cadd-1001-4673-8a17-5d6053199eb7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
