<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Ya, Masuk</name>
   <tag></tag>
   <elementGuidId>6cfc8a72-7359-4cb8-8ae7-818b56ed252c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Nomor HP Sudah Terdaftar'])[1]/following::span[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.css-lwa81l-unf-btn.eg8apji0 > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>9f27dfd3-deaf-41c3-b1e0-3aee34c4558d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ya, Masuk</value>
      <webElementGuid>96128506-42d5-4953-beb4-5235e3941d40</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;css-18e1z3z&quot;]/body[1]/div[3]/div[@class=&quot;css-17b17is-unf-dialog ef541p40&quot;]/div[@class=&quot;css-1oqklfy ef541p41&quot;]/button[@class=&quot;css-lwa81l-unf-btn eg8apji0&quot;]/span[1]</value>
      <webElementGuid>eb790e29-4d1a-4cf3-95b3-5bef57bf6fbe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nomor HP Sudah Terdaftar'])[1]/following::span[1]</value>
      <webElementGuid>4d70ba38-42ee-442b-8f2b-155f7123dee1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ubah'])[1]/preceding::span[1]</value>
      <webElementGuid>62d53243-a9d8-4582-aa71-19bd661c293f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Ya, Masuk']/parent::*</value>
      <webElementGuid>df58aea5-927d-4b9d-9e12-9a3b674c5f5f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button/span</value>
      <webElementGuid>55177bea-cc1c-4b77-9317-d0d398fa000a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Ya, Masuk' or . = 'Ya, Masuk')]</value>
      <webElementGuid>012b7cda-3dd9-4ae3-9ee2-019cb24c5975</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
