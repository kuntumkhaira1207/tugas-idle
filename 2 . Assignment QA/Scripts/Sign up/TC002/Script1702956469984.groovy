import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Sign up/TC001'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Page_Daftar  Tokopedia/input_Phone Number or Email_regis-input'), '088225931404')

WebUI.click(findTestObject('Object Repository/Page_Daftar  Tokopedia/span_Daftar'))

WebUI.click(findTestObject('Object Repository/Page_Daftar  Tokopedia/span_Ya, Benar'))

WebUI.click(findTestObject('Object Repository/Page_Verifikasi/button_Gunakan Metode SMS'))

WebUI.setText(findTestObject('Object Repository/Page_Verifikasi/input_Kode verifikasi telah dikirim melalui_5defe3'), '929346')

WebUI.setText(findTestObject('Object Repository/Page_Daftar  Tokopedia/input_Nama Lengkap_regis-input'), 'Ezi Cantika')

WebUI.click(findTestObject('Object Repository/Page_Daftar  Tokopedia/span_Selesai'))

